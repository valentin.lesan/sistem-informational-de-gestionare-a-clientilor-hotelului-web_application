<?php
	require_once("connection.php");
	session_start();
	if(!isset($_SESSION['username'])){
		header("Location:login.php");
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Operațiuni DB
		</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="utilities/css/data_style.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header" class="alt">
				<div class="logo"><a href="home.php">Hotel <span></span></a></div>
				<a href="#menu">Menu</a>
			</header>

	<!-- Panou Navigare -->
		<nav id="menu">
			<ul class="links">
				<li><a href="home.php">Home</a></li>
				<li><a href="second_page.php">Camerele Hotelului</a></li>
				<?php
				if(isset($_SESSION['username'])){
					echo '<li><a href="logout.php">Log out</a></li>';
				} else echo '<li><a href="login.php">Login</a></li>';
				?>
				<li><a>Operații DB</a></li>
				<li>
					<a> <form action="data.php" method="POST"> <input type="submit" name="trial1" value="Afișează toți turiștii"> </form></a> 	
				</li>	
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial2" value="Lista turiștilor cazați"> </form></a> 	
				</li>	
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial3" value="Lista tipurilor camerelor de hotel"> </form></a> 	
				</li>	
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial4" value="Cea mai scumpă cameră de hotel"> </form></a> 	
				</li>	
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial5" value="Cea mai ieftină cameră de hotel"> </form></a> 	
				</li>	
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial6" value="Lista turiștilor cazați ce dispun de reducere"> </form></a> 	
				</li>
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial7" value="Turiștii cu șederea expirată la data specificată"> </form></a> 	
				</li>
				<li>
					<a> <form action="" method="POST"> <input type="submit" name="trial8" value="Turiștii și sejurul ales"> </form></a> 	
				</li>
			</ul>
		</nav>

			<!-- Prima secțiune -->

            <section id="banner">
                <h1>
					Welcome <?php
					 if(isset($_SESSION['username'])){
						echo $_SESSION['username'];
					} ?>
					</h1>
				<p>Aici gestionați clienții hotelului.</p>
			</section>

			<!-- A 2-a secțiune + gestionare-->

			<section id="banner2">
			<?php
			if(isset($_POST['trial1'])){
				echo "<h3 style='color:white;'>Lista tuturor turiștilor</h3>";
				$query="SELECT * FROM v_Turisti";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>IdTurist</th><th>Nume</th><th>Prenume</th><th>Patronimic</th><th>Sex Turist</th><th>Telefon</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["IdTurist"]. "</td><td>". $row["NumeTurist"]. "</td><td>" . $row["PrenumeTurist"] ."</td><td>" .$row["PatrTurist"] ."</td><td>" .$row["SexTurist"]. "</td><td>" .$row["Telefon"]. "</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial2'])){
				echo "<h3 style='color:white;'>Lista turiștilor cazați</h3>";
				$query="SELECT * FROM v_data_expirarii";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>IdInregistrare</th><th>IdCamera</th><th>IdTurist</th><th>Data închirierii</th><th>Termenul cazării</th><th>Termen Final</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["idinregistrare"]. "</td><td>". $row["IdCamera"]. "</td><td>" . $row["IdTurist"] ."</td><td>" .$row["Data_inchirierii"] ."</td><td>" .$row["termen_total"]. "</td><td>" .$row["Termen_Final"]. "</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial3'])){
				echo "<h3 style='color:white;'>Lista tipurilor camerelor de hotel</h3>";
				$query="SELECT * FROM v_Camera_type";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>Descriere</th><th>Capacitate maximă</th><th>Preț/zi</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["Descriere"]. "</td><td>" . $row["Capacitate_Max"] ."</td><td>" .$row["Pret_Zi"] ."</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial4'])){
				echo "<h3 style='color:white;'>Cea mai scumpă cameră de hotel</h3>";
				$query="SELECT Descriere,Pret_Zi FROM v_Camera_type WHERE Pret_Zi = (SELECT Max(Pret_Zi) FROM v_Camera_type)";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>Descriere</th><th>Preț/zi</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["Descriere"]. "</td><td>". $row["Pret_Zi"]. "</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial5'])){
				echo "<h3 style='color:white;'>Cea mai ieftină cameră de hotel</h3>";
				$query="SELECT Descriere,Pret_Zi FROM v_Camera_type WHERE Pret_Zi=(SELECT MIN(Pret_Zi) FROM v_Camera_type)";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>Descriere</th><th>Preț/zi</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["Descriere"]. "</td><td>". $row["Pret_Zi"]. "</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial6'])){
				echo "<h3 style='color:white;'>Lista turiștilor cazați ce dispun de reducere</h3>";
				$query="SELECT * FROM v_CALCUL_Total ORDER BY IdTurist";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>Nume</th><th>Prenume</th><th>Preț final</th><th>Preț calculat cu reducere</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["NumeTurist"]. "</td><td>". $row["PrenumeTurist"]. "</td><td>".$row["Pret_final"]."</td><td>".$row["Pret_Calculat_Reducere"]."</td></tr>";
					}
					echo "</table></div>";
				}
			} else  if(isset($_POST['trial7'])){
				echo "<h3 style='color:white;'>Lista tuturor turiștilor cu șederea expirată la data specificată</h3>";
				echo '<form action="trial7.php" method="post"><p style="display:inline"> Dați data:</p> 
						<input type = "text" name = "data" placeholder="24.04.2020">
						<input type = "submit" value = "OK">	
						</form>';
			}  else if(isset($_POST['trial8'])){
				echo "<h3 style='color:white;'>Lista turiștilor și sejurul ales</h3>";
				$query="SELECT * FROM v_Show_Sejur";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>Nume</th><th>Prenume</th><th>Denumire Sejur</th><th>Data sejurului</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["NumeTurist"]. "</td><td>". $row["PrenumeTurist"]. "</td><td>".$row["Denumire"]."</td><td>".$row["Data_Sejur"]."</td></tr>";
					}
					echo "</table></div>";
				}
			} else {
				echo "<p>Selectați o operațiune din meniu</p>";
			}

			?>
			
			</section>

	<!-- Footer -->
	<footer id="footer">
			<div class="inner">
				<div class="flex">
					<div class="copyright">
						&copy; codeskills (Leșan V.) All rights reserved.
					</div>
					<ul class="icons">
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
						<li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
						<li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>
					</ul>
				</div>
			</div>
		</footer>

			
	<!-- Biblioteci script-uri -->
		<script src="utilities/js/jquery.min.js"></script>
		<script src="utilities/js/jquery.scrollex.min.js"></script>
		<script src="utilities/js/skel.min.js"></script>
		<script src="utilities/js/util.js"></script>
		<script src="utilities/js/gallery.js"></script>
	</body>
</html>