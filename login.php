<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="utilities/css/login_design.css">
    <title>Login Page</title>
    <script src="utilities/js/jquery.min.js"></script>
</head>
<body>
<div class="login-page">
  <div class="form">
    <form class="register-form" action = "insert.php" method = "post">
      <input type="text" name = "username" placeholder="Username" required/>
      <input type="password" name = "pass" placeholder="Password" required/>
      <button name = "Register">Înregistrare</button>
      <p class="message">Ești înregistrat? <a>Logare</a></p>
    </form>
    <form class="login-form" action = "log_in.php" method = "post">
      <input type="text" name = "username2" placeholder="Username" required/>
      <input type="password" name = "pass2" placeholder="Password" required/>
      <button name = "Logare">Logare</button>
      <p class="message">Nu ești înregistrat? <a>Creare cont</a></p>
    </form>
  </div>
</div>
<script>
      $('.message > a').click(
      function(){
      $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    });
    </script>
</body>
</html>