<?php
	require_once("connection.php");
	session_start();
	if(!isset($_SESSION['username'])){
		header("Location:login.php");
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Lista turiștilor...
		</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="utilities/css/data_style.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header" class="alt">
				<div class="logo"><a href="data.php" style="font-size:3rem">&larr;</a></div>
			</header>

			<!-- Prima secțiune -->
            <section id="banner">
                <h1>
					Welcome <?php
					 if(isset($_SESSION['username'])){
						echo $_SESSION['username'];
					}?>
					</h1>
				<p>Aici gestionați clienții hotelului.</p>
			</section>

			<!-- A 2-a secțiune + gestionare -->
			<section id="banner2">
			<?php
				if(isset($_POST['data'])){
                $var = $_POST['data'];
                $time = strtotime($var);
                $data = date('Y-m-d',$time);
				$query="SELECT v_Turisti.IdTurist, NumeTurist, PrenumeTurist, PatrTurist, SexTurist, Telefon, Termen_Final FROM v_Turisti
				INNER JOIN inregistrare ON inregistrare.idturist = v_Turisti.idturist
				INNER JOIN  v_Data_expirarii ON v_Data_expirarii.IdTurist = v_Turisti.IdTurist
				WHERE '$data' = v_Data_expirarii.Termen_Final;";
				$result = mysqli_query($conexiune, $query);
				if (mysqli_num_rows($result) != 0) {
					echo "<div class='overflow-x:auto'><table><tr><th>IdTurist</th><th>Nume</th><th>Prenume</th><th>Patronimic</th><th>Sex Turist</th><th>Telefon</th><th>Termen Final</th></tr>";
					while($row = mysqli_fetch_assoc($result)) {
						echo "<tr><td>". $row["IdTurist"]. "</td><td>". $row["NumeTurist"]. "</td><td>" . $row["PrenumeTurist"] ."</td><td>" .$row["PatrTurist"] ."</td><td>" .$row["SexTurist"]. "</td><td>" .$row["Telefon"]. "</td><td>".$row["Termen_Final"]."</td></tr>";
					}
					echo "</table></div>";
                } else {
                    echo "<p style='font-size:1.5em;'>Nu sunt turiști ce coincid cu data expirării introdusă de dvs, sau nu ați introdus informații.</p>";
                } 
                } else header("Location:data.php");
			?>
			
			</section>

		<!-- Footer -->
	<footer id="footer">
		<div class="inner">
			<div class="flex">
				<div class="copyright">
					&copy; codeskills (Leșan V.) All rights reserved.
				</div>
					<ul class="icons">
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
						<li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
						<li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>
					</ul>
			</div>
		</div>
	</footer>
	</body>
</html>