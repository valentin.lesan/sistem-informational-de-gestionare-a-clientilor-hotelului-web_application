<?php
require_once("connection.php");
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Camerele Hotelului
	</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="utilities/css/2style.css" />
</head>
<body>

	<!-- Header -->
		<header id="header" class="alt">
			<div class="logo"><a href="home.php">Hotel <span></span></a></div>
			<a href="#menu">Menu</a>
		</header>

	<!-- Panou Navigare -->
		<nav id="menu">
			<ul class="links">
				<li><a href="home.php">Home</a></li>
				<li><a href="second_page.php">Camerele Hotelului</a></li>
				<?php
				if(isset($_SESSION['username'])){
					echo '<li><a href="logout.php">Log out</a></li>';
					echo '<li><a href="data.php">Operațiuni DB</a></li>';
				} else echo '<li><a href="login.php">Login</a></li>';
				?>
			</ul>
		</nav>

	<!-- Banner -->
		<section class="banner full">
		
			<article>
				<img src="images/slide1.jpg" alt="" />
				<div class="inner">
					<header>
						<p>Filialele hotelului</p>
						<h2>Tahiti</h2>
					</header>
				</div>
			</article>
			<article>
				<img src="images/slide2.jpg" alt="" />
				<div class="inner">
					<header>
						<p>Filialele hotelului</p>
						<h2>Maldive</h2>
					</header>
				</div>
			</article>
			<article>
				<img src="images/slide3.jpg"  alt="" />
				<div class="inner">
					<header>
						<p>Filialele hotelului</p>
						<h2>Honolulu, Hawaii</h2>
					</header>
				</div>
			</article>
			<article>
				<img src="images/slide4.jpg"  alt="" />
				<div class="inner">
					<header>
						<p>Filialele hotelului</p>
						<h2>București</h2>
					</header>
				</div>
			</article>
			<article>
				<img src="images/slide5.jpg"  alt="" />
				<div class="inner">
					<header>
						<p>Filialele hotelului</p>
						<h2>Insulele Canare</h2>
					</header>
				</div>
			</article>
		</section>

				
	
	<!-- Prima secțiune -->
		<section id="one" class="wrapper style2">
			<div class="inner">
				<div class="grid-style">

					<div>
						<div class="box">
							<div class="image fit">
								<img src="images/room1.jpg" alt="" />
							</div>
							<div class="content">
								<header class="align-center">
									<p>Camerele hotelului</p>
									<h2>Camera Super-Deluxe</h2>
								</header>
								<p> Comfortul este o obișnuință când vine vorba de aceste camere. Ai totul inclus, ca șederea ta să fie de neuitat. Acum, dispui și de reduceri speciale, așa că nu ezita!</p>
								<footer class="align-center">
									<a href="#" class="button alt">Learn More</a>
								</footer>
							</div>
						</div>
					</div>

					<div>
						<div class="box">
							<div class="image fit">
								<img src="images/room2.jpg" alt="" />
							</div>
							<div class="content">
								<header class="align-center">
									<p>Camerele hotelului</p>
									<h2>Camera dublă standard </h2>
								</header>
								<p>Să fii bine odihnit și să ai o poftă nebună de a-ți continua vacanța este un alt mod de a alege această cameră. Vă garantăm condiții impecabile, pentru un preț de senzație!</p>
								<footer class="align-center">
									<a href="#" class="button alt">Learn More</a>
								</footer>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

	<!-- A 2-a secțiune -->
		<section id="two" class="wrapper style3">
			<div class="inner">
				<header class="align-center">
					<p>Mai multe detalii despre camerele hotelului puteți vedea mai jos</p>
				</header>
			</div>
		</section>

	<!-- A 3-a secțiune -->
		<section id="three" class="wrapper style2">
			<div class="inner">
				<header class="align-center">
					<p class="special">Puține hoteluri se ridică la asemenea standarde</p>
					<h2>O cameră de hotel, pentru fiecare necesitate!</h2>
				</header>
				<div class="gallery">
					<div class="container">
						<div class="image fit">
							<a href="#"><img src="images/foto_room1.jpg" alt="" /></a>
							<div class="overlay">
							<div class="text">
							Cameră Standard Twin
							</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="image fit">
							<a href="#"><img src="images/foto_room2.jpg" alt="" /></a>
							<div class="overlay">
							<div class="text">
							Cameră Super Deluxe
							</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="image fit">
							<a href="#"><img src="images/foto_room3.jpg" alt="" /></a>
							<div class="overlay">
							<div class="text">
							Cameră Junior Suite
							</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="image fit">
							<a href="#"><img src="images/foto_room4.jpg" alt="" /></a>
							<div class="overlay">
							<div class="text">
							Cameră Euro Twin
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>



	<!-- Footer -->
	<footer id="footer">
			<div class="inner">
				<div class="flex">
					<div class="copyright">
						&copy; codeskills (Leșan V.) All rights reserved.
					</div>
					<ul class="icons">
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
						<li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
						<li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>
					</ul>
				</div>
			</div>
		</footer>

		
	<!-- Biblioteci script-uri -->
		<script src="utilities/js/jquery.min.js"></script>
		<script src="utilities/js/jquery.scrollex.min.js"></script>
		<script src="utilities/js/skel.min.js"></script>
		<script src="utilities/js/util.js"></script>
		<script src="utilities/js/gallery.js"></script>
</body>
</html>