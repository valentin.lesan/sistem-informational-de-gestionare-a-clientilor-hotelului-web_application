<?php
	require_once("connection.php");
	session_start();
	?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Home</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="utilities/css/main.css" />
	</head>
	<body>
	<!-- Header -->
	<header id="header" class="alt">
		<div class="logo"><a href="home.php">Hotel</a></div>
		<a href="#menu">Menu</a>
	</header>

	<!-- Panou Navigare -->
		<nav id="menu">
		<ul class="links">
		<li><a href="home.php">Home</a></li>
			<li><a href="second_page.php">Camerele Hotelului</a></li>
			<?php
			if(isset($_SESSION['username'])){
				echo '<li><a href="logout.php">Log out</a></li>';
				echo '<li><a href="data.php">Operațiuni DB</a></li>';
			} else echo '<li><a href="login.php">Login</a></li>';
			?>		
		</ul>
	</nav>


	<!-- Banner -->
	<section id="banner">
		<h1>
			Welcome <?php
				if(isset($_SESSION['username'])){
				echo $_SESSION['username'];
			} ?>
			</h1>
		<p>Aplicația de gestionare a clienților hotelului.</p>
	</section>

	<!-- Prima secțiune -->
	<section id="one" class="wrapper">
		<div class="inner">
			<div class="flex flex-3">
				<article>
					<header>
						<h3 class="align-center">Despre această aplicație</h3>
					</header>
					<p>Aplicația dată este destinată gestionării clienților hotelului. Produsul respectiv este recomandat de a fi utilizat doar de catre personalul hotelului.</p>
					<footer class="align-center">
						<a href="#" class="button special">Mai mult</a>
					</footer>
				</article>
				<article>
					<header>
						<h3 class="align-center">Actualitatea produsului respectiv</h3>
					</header>
					<p>Orice agenție hotelieră sau intreprindere din domeniul hotelier are nevoie de o gestionare facilitată a clienților hotelului.</p>
					<footer class="align-center">
						<a href="#" class="button special">Mai mult</a>
					</footer>
				</article>
				<article>
					<header>
						<h3 class="align-center	">Platforme de dezvoltare</h3>
					</header>
					<p>Produsul software respectiv aparține domeniului web și este foarte ușor de accesat și de adaptat la orice sistem al oricărei organizații.</p>
					<footer class="align-center">
						<a href="#" class="button special">Mai mult</a>
					</footer>
				</article>
			</div>
		</div>
	</section>

	<!-- A 2-a secțiune -->
	<section id="two" class="wrapper style1 special">
		<div class="inner">
			<header>
				<h2>Personalul hotelului</h2>
				<p>Angajații lunii</p>
			</header>
			<div class="flex flex-4">
				<div class="box person">
					<div class="image round">
						<img src="images/pic_avatar1.png" alt="Person 1" />
					</div>
					<h3>Costel</h3>
					<p>Barman</p>
				</div>
				<div class="box person">
					<div class="image round">
						<img src="images/pic_avatar2.png" alt="Person 2" />
					</div>
					<h3>Irina</h3>
					<p>Manager Relații-Clienți</p>
				</div>
				<div class="box person">
					<div class="image round">
						<img src="images/pic_avatar.png" alt="Person 3" />
					</div>
					<h3>Gheorghe</h3>
					<p>Contabil</p>
				</div>
				<div class="box person">
					<div class="image round">
						<img src="images/pic_avatar3.png" alt="Person 4" />
					</div>
					<h3>Andrei</h3>
					<p>Departamentul IT</p>
				</div>
			</div>
		</div>
	</section>

	<!-- A 3-a secțiune -->
	<section id="three" class="wrapper special">
		<div class="inner">
			<header class="align-center">
				<h2>Articole importante</h2>
				<p>Funcționalitatea hotelului</p>
			</header>
			<div class="flex flex-2">
				<article>
					<div class="image fit">
						<img src="images/final_article1.jpg" alt="Pic 01" />
					</div>
					<header>
						<h3>Cum lucrează acest site</h3>
					</header>
					<p>Acest soft, este destinat mai mult gestionării interne decât accesului larg al oricărei persoane. Nu este un site al hotelului, ci mai degrabă un produs atractiv pentru personalul acestuia, pentru ca lucrul cu clienții hotelului să fie nu numai ușor, dar și plăcut vizual.</p>
					<footer class="align-center">
						<a href="#" class="button special">Mai mult</a>
					</footer>
				</article>
				<article>
					<div class="image fit">
						<img src="images/final_article2.jpg" alt="Pic 02" />
					</div>
					<header>
						<h3>Este bine ca orice vizitator să acceseze acest site?</h3>
					</header>
					<p>Tocmai din aceste considerente și am creat o funcționalitate exclusivă pentru personalul hotelului. Doar aceștia vor putea să se logheze și să dispună de o gamă largă de operațiuni cu cei cazați în hotelul respectiv. Cu toate acestea, am creat un design atractiv, și pentru vizitatorii acestui hotel. Ei vor avea evident doar posibilitatea de a vedea detalii și articole destinate publicului larg.</p>
					<footer class="align-center">
						<a href="#" class="button special">Mai mult</a>
					</footer>
				</article>
			</div>
		</div>
	</section>

	<!-- Footer -->
	<footer id="footer">
		<div class="inner">
			<div class="flex">
				<div class="copyright">
					&copy; codeskills (Leșan V.) All rights reserved.
				</div>
				<ul class="icons">
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
					<li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
					<li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>
				</ul>
			</div>
		</div>
	</footer>

	<!-- Biblioteci script-uri -->
		<script src="utilities/js/jquery.min.js"></script>
		<script src="utilities/js/skel.min.js"></script>
		<script src="utilities/js/util.js"></script>
		<script src="utilities/js/gallery.js"></script>
	</body>
	</html>